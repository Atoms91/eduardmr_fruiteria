import React from "react";
import styled from 'styled-components';

const DivTotal = styled.div`
    height: 50px;
    width: 300px; 
    border: 1px solid black;
`;
const DivFruta = styled.div`
height: 100px;
width: 300px; 
line-height: 50px;
background-color: green;
color:white;
`;

export default (props) => {
    let total = 0; 
    const compra = props.fruita.map(el => (
        
            <DivFruta key={el.id}>
                <p>{el.nom}</p>
                <p>{el.cantidad + 'u x ' + el.preu + '€/u =' + (el.cantidad*el.preu)}</p>
                {total = total + (el.cantidad*el.preu)}
            </DivFruta>
        )
    );
  
    return (
        <>  
            {compra}
                
            <DivTotal>
                <h2>{'Total: '+total+'€'}</h2>
            </DivTotal>
        </>
    );
}