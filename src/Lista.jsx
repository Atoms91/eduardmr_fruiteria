import React, { useState } from "react";
import Productos from './Productos';
import styled from 'styled-components';
import Ticket from "./Ticket";
let estilo = {display:"inline-block"}
let datos = Productos.map(el => {
    el.cantidad = 0;
    return el
});

const DivFruta = styled.div`
    height: 50px;
    width: 300px; 
    background-color: green;
    color:white;
`;

const BotonFruta = styled.button`
    float:right;
    margin-top -20px;
`;


export default () => {
    
    const [fruta, setFruta] = useState(datos);
    let total = 0;


    const lista = fruta.map(el => (
        <DivFruta key={el.id} className="">
            <p>{el.nom + ' (' + el.preu + '€/u)' + el.cantidad}</p>
            <BotonFruta onClick={() => Afegir(el.id)}>Afegir</BotonFruta>
        </DivFruta>
    )
    );
    const Afegir = (props) => {
        const nuevalista = fruta.map(el => {
            if (props === el.id) { el.cantidad = el.cantidad + 1;}
            return el;
        })
        setFruta(nuevalista);
    }
    return (
        <>
        

            <h1>Fruiteria</h1>
           
            <div style={estilo}>
                        {lista}
            </div>
                    <div style={estilo}>
                        <Ticket fruita={fruta}/>
                    </div>
  
           
        </>
    );
}